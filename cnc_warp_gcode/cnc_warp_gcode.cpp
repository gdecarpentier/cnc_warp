// ----------------------------------------------------------------------
// cnc_warp_gcode. Giliam de Carpentier, 2017. http://www.decarpentier.nl
// ----------------------------------------------------------------------
// See INSTRUCTIONS.TXT for details on what it does and how to use it.
//
// BUILDING FROM SOURCE CODE
// =========================
// Use the Visual Studio 2015 "cnc_warp.sln" to build it.
//
// SOURCE CODE DEPENDENCIES
// ========================
// - Eigen: A third-party C++ linear algebra library. LGPL licensed. http://eigen.tuxfamily.org/.
//
// LICENSE AND WARRANTY
// ====================
// This code is GPL v3 licensed and may be used freely under its terms. This software comes without any warranty, 
// so use it at your own risk. I am not liable for any damage caused directly or indirectly by this software.


// Include headers
#include <fstream>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <cstring>
#include "eigen/Eigen/Dense"



// Typedefs and namespaces
using Eigen::MatrixXd;
typedef std::vector<std::vector<std::string> > tokens_t;
typedef Eigen::VectorXd warp_params_t;
typedef std::vector<Eigen::Vector3d> probe_points_t;



// Configuration globals
static const float s_max_cluster_distance	= 1.0;



// Get a new set of probe points such that left-front-most probe is at XY coordinate (0, 0).
probe_points_t get_local_probe_points(const probe_points_t& probe_points)
{
	Eigen::Vector3d offset(std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), 0.0);
	for (size_t i = 0; i < probe_points.size(); ++i)
	{
		Eigen::Vector3d p = probe_points[i];
		if (p.x() < offset.x() && p.y() < offset.y())
			offset = p;
	}

	offset.z() = 0.0;

	probe_points_t local_probe_points(probe_points.size());

	for (size_t i = 0; i < probe_points.size(); ++i)
	{
		local_probe_points[i] = probe_points[i] - offset;
	}

	return local_probe_points;
}



// Evalulate the warped surface at the given XY coordinate.
// We assume the local z is relative to the fitted surface at (0,0), so we ignore warp_params[0]
double get_warped_z(const warp_params_t& warp_params, double x, double y)
{
	if (warp_params.size() == 4)
	{
		// Evaluate a bilinear surface.
		return /*warp_params[0] +*/ warp_params[1] * x + warp_params[2] * y + 
		   warp_params[3] * x * y;
	}
	else
	{
		// Evaluate a quadratic surface.
		return /*warp_params[0] +*/ warp_params[1] * x + warp_params[2] * y + 
		   warp_params[3] * x * x + warp_params[4] * x * y + warp_params[5] * y * y;
	}
}



// Find the bilinear surface parameters that best the given probe points
warp_params_t fit_warp_params_bilinear(const probe_points_t& probe_points)
{
	Eigen::MatrixXd a(probe_points.size(), 4);
	Eigen::VectorXd b(probe_points.size());
	for (size_t i = 0; i < probe_points.size(); ++i)
	{
		// probe position relative to last probe
		Eigen::Vector3d p = probe_points[i];

		a(i, 0) = 1;
		a(i, 1) = p.x();
		a(i, 2) = p.y();
		a(i, 3) = p.x() * p.y();
		b[i]	= p.z();
	}

	// Solve the least-squares problem.
	return a.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(b);
}


// Find the quadratic surface parameters that best the given probe points
warp_params_t fit_warp_params_quadratic(const probe_points_t& probe_points)
{
	Eigen::MatrixXd a(probe_points.size(), 6);
	Eigen::VectorXd b(probe_points.size());
	for (size_t i = 0; i < probe_points.size(); ++i)
	{
		Eigen::Vector3d p = probe_points[i];

		a(i, 0) = 1;
		a(i, 1) = p.x();
		a(i, 2) = p.y();
		a(i, 3) = p.x() * p.x();
		a(i, 4) = p.x() * p.y();
		a(i, 5) = p.y() * p.y();
		b[i]	= p.z();
	}

	// Solve the least-squares problem.
	return a.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(b);
}



// Load the probe measurements from file into an array of XYZ coordinates
probe_points_t load_probe_points(const char* filename)
{
	probe_points_t result;

	std::ifstream file_stream;

	file_stream.open(filename);
	if (file_stream.fail())
	{
		std::cout << "Error while loading '" << filename << "'\n";
		return result;
	}

	std::string line;
	while (std::getline(file_stream, line))
	{
		std::istringstream line_stream(line);
		double x, y, z;
		if (!(line_stream >> x >> y >> z))
		{
			std::cout << "Error while parsing line '" << line << "'\n";
		}

		result.push_back(Eigen::Vector3d(x, y, z));
	}

	file_stream.close();

	return result;
}



// Get a new set of probe points with only one measurement per XY location.
// If multiple measurements per location are available, take the median.
probe_points_t get_filtered_probe_points(const probe_points_t& probe_points)
{
	// Group all measurements into clusters. Each (clearly) different XY locations gets its own cluster
	typedef std::pair<Eigen::Vector3d, probe_points_t>  probe_cluster_t;
	std::vector<probe_cluster_t> probe_clusters;
	for (size_t i = 0; i < probe_points.size(); ++i)
	{
		const Eigen::Vector3d& probe_point = probe_points[i];

		size_t cluster_index = 0;
		for (; cluster_index < probe_clusters.size(); ++cluster_index)
		{
			Eigen::Vector3d cluster_point = probe_clusters[cluster_index].first / probe_clusters[cluster_index].second.size();

			Eigen::Vector2d delta_2d(probe_point.x() - cluster_point.x(), probe_point.y() - cluster_point.y());
			if (delta_2d.squaredNorm() < s_max_cluster_distance * s_max_cluster_distance)
			{
				 probe_clusters[cluster_index].first += probe_point;
				 probe_clusters[cluster_index].second.push_back(probe_point);

				 break;
			}
		}

		if (cluster_index == probe_clusters.size())
		{
			probe_cluster_t cluster;
			cluster.first = probe_point;
			cluster.second.push_back(probe_point);
			probe_clusters.push_back(cluster);
		}
	}

	// Output one measurement per cluster. If a cluster contains more one than one measurment, use its median position.
	probe_points_t result_points;
	for (size_t cluster_index = 0; cluster_index < probe_clusters.size(); ++cluster_index)
	{
		probe_cluster_t& cluster = probe_clusters[cluster_index];
		std::sort(
			cluster.second.begin(), 
			cluster.second.end(), 
			[](const Eigen::Vector3d& a, const Eigen::Vector3d& b)
			{
				return a.z() < b.z();
			});

		int probe_count = (int)cluster.second.size();
		int ignore_count = std::max(probe_count - 3, 0) / 2;
		Eigen::Vector3d sum(0.0f, 0.0f, 0.0f);
		for (int i = ignore_count; i < probe_count - ignore_count; ++i)
		{
			sum += cluster.second[i];
		}

		sum /= probe_count - 2 * ignore_count;
		result_points.push_back(sum);
	}

	return result_points;
}



// Load a file and split it into GCode lines.
tokens_t load_gcode_tokens(const char* filename)
{
	tokens_t result;

	std::ifstream file_stream;

	file_stream.open(filename);
	if (file_stream.fail())
	{
		std::cout << "Error while loading '" << filename << "'\n";
		return result;
	}
	
	std::string line;
	while (std::getline(file_stream, line))
	{
		std::istringstream line_stream(line);
		std::vector<std::string> line_tokens;

		std::string token;
		while (line_stream >> token)
			line_tokens.push_back(token);

		result.push_back(line_tokens);
	}

	file_stream.close();

	return result;
}



// Store the given GCode lines into a file.
void save_gcode_tokens(const char* filename, const tokens_t& tokens)
{
	std::ofstream file_stream;

	file_stream.open(filename);
	if (file_stream.fail())
	{
		std::cout << "Error while saving '" << filename << "'\n";
		return;
	}

	for each (const std::vector<std::string>& line_tokens in tokens)
	{
		for (size_t i = 0; i < line_tokens.size(); ++i)
			file_stream << line_tokens[i] << (i < line_tokens.size() - 1 ? ' ' : '\n');
	}

	file_stream.close();
}


// Transform the GCode lines using the given warp surface.
tokens_t warp_gcode_tokens(const warp_params_t& warp_params, const tokens_t& tokens)
{
	tokens_t warped_tokens;

	bool found_g20 = false; // in inches
	bool found_g21 = false; // in millimeters
	bool found_g90 = false; // absolute coord mode
	bool found_g91 = false; // relative coord mode
	bool found_ijk = false; // specified i, j or k coordinate
	bool found_multiple_xyz = false; // specified more than one x, y, or z per line
	double x = 0, y = 0, z = 0;

	// Parse each line
	for (size_t line_index = 0; line_index < tokens.size(); ++line_index)
	{
		const std::vector<std::string>& line_tokens = tokens[line_index];
		std::vector<std::string> warped_line_tokens = line_tokens;

		int x_index = -1, y_index = -1, z_index = -1;

		// Parse each token on this line
		for (size_t line_token_index = 0; line_token_index < line_tokens.size(); ++line_token_index)
		{
			std::istringstream token_stream(line_tokens[line_token_index]);
			char letter;
			token_stream >> letter;

			// Parse first letter of this token
			switch (toupper(letter))
			{
				case 'I':
				case 'J':
				case 'K':
				{
					// Found an I, J or K radial parameter, which are currently not supported.
					found_ijk = true;
					break;
				}

				case 'G':
				{	
					// Found a new G command.
					int code;
					token_stream >> code;
					switch (code)
					{
						case 20: found_g20 = true; break;
						case 21: found_g21 = true; break;
						case 90: found_g90 = true; break;
						case 91: if (token_stream.peek() != '.') found_g91 = true; break;
						default: break;
					}
					break;
				}

				case 'X':
				{
					// Found an X coordinate.
					if (x_index >= 0) found_multiple_xyz = true;
					token_stream >> x;
					x_index = line_token_index;
					break;
				}

				case 'Y':
				{
					// Found an Y coordinate.
					if (y_index >= 0) found_multiple_xyz = true;
					token_stream >> y;
					y_index = line_token_index;
					break;
				}
			
				case 'Z':
				{
					// Found an Z coordinate.
					if (z_index >= 0) found_multiple_xyz = true;
					token_stream >> z;
					z_index = line_token_index;
					break;
				}
			}
		}

		if (z_index < 0 && (x_index >= 0 || y_index >= 0))
		{
			// If this line has an X or Y coordinate but not a Z coordinate, then add it.
			z_index = std::max(x_index, y_index) + 1;
			warped_line_tokens.insert(warped_line_tokens.begin() + z_index, std::string());
		}

		if (z_index >= 0)
		{
			// If this (now has) a Z coordinate, adjust it using the given warped surface.
			double delta_z = get_warped_z(warp_params, x, y);
			warped_line_tokens[z_index] = "Z" + std::to_string(z + delta_z);
		}

		warped_tokens.push_back(warped_line_tokens);
	}

	// Check for unsupported GCodes.

	if (found_g91 /*|| !found_g90*/)
		std::cout << "WARNING: GCode not specified (everywhere) in absolute coordinates. This will probably lead to incorrect results\n";
	
	if (found_g20 || !found_g21)
		std::cout << "ERROR: GCode not specified (everywhere) in millimeters. This will probably lead to incorrect results\n";
	
	if (found_ijk)
		std::cout << "ERROR: GCode contains I, J and/or K coordinates for spherical interpolation. This will probably lead to incorrect results\n";
		
	if (found_multiple_xyz)
		std::cout << "ERROR: GCode contains multiple X, Y and/or Z coordinates per line. This will probably lead to incorrect results\n";

	return warped_tokens;
}


// Entry point to the application
int main(int argc, char** argv)
{
	// Verify that the given command-line arguments fits the expected format.

	if (argc != 4)
	{
		std::cout << "SYNTAX ERROR: Expected cnc_warp_gcode.exe <probe_measurements_filename> <gcode_input_filename> <gcode_output_filename>\n";
		std::cout << "Giliam de Carpentier, 2017. See http://www.decarpentier.nl or https://bitbucket.org/gdecarpentier/cnc_warp for details.\n";
		return -1;
	}

	const char* probe_measurements_filename = argv[1];
	const char* gcode_input_filename = argv[2];
	const char* gcode_output_filename = argv[3];

	// Load the probe measurements.
	probe_points_t global_probe_points(load_probe_points(probe_measurements_filename));

	// Transform them into local space, assuming the left-front-most probe represents the height at XY coordinate (0,0).
	probe_points_t local_probe_points(get_local_probe_points(global_probe_points));

	// Combine any probe measurements that are closeby into a single median coordinate, filtering out probe errors as much as possible.
	probe_points_t filtered_probe_points(get_filtered_probe_points(local_probe_points));

	// Fit the probe data to a warped surface.
	//warp_params_t warp_params(fit_warp_params_bilinear(filtered_probe_points));
	warp_params_t warp_params(fit_warp_params_quadratic(filtered_probe_points));

	// Dump the coefficients to the fitted warp surface.
	std::cout << "Surface parameters:" << std::endl;
	for (int i = 0; i < warp_params.size(); ++i)
	{
		if (i != 0)
			std::cout << ", ";
		std::cout << std::setw(12) << warp_params[i];
	}

	std::cout << std::endl;

	// Dump the error between the actual probe measurements and the fitted surface.
	std::cout << "Surface estimation rel. to last probe:" << std::endl;
	for (size_t i = 0; i < local_probe_points.size(); ++i)
	{
		const Eigen::VectorXd& p = local_probe_points[i];
		double z_est = get_warped_z(warp_params, p.x(), p.y()) + warp_params[0]; 
		std::cout << std::setw(12) << p.x() << "," << std::setw(12) << p.y() <<  ": " << std::setw(12) << p.z() <<  " -> err: " << std::setw(12) << (z_est - p.z()) << std::endl;
	}

	// Get the GCode to adjust from the given input file.
	tokens_t tokens = load_gcode_tokens(gcode_input_filename);

	// Adjust the GCode.
	tokens_t warped_tokens = warp_gcode_tokens(warp_params, tokens);

	// Save the adjusted GCode to the given output file.
	save_gcode_tokens(gcode_output_filename, warped_tokens);
}