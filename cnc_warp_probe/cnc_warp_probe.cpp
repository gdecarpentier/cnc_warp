// ----------------------------------------------------------------------
// cnc_warp_probe. Giliam de Carpentier, 2017. http://www.decarpentier.nl
// ----------------------------------------------------------------------
// See INSTRUCTIONS.TXT for details on what it does and how to use it.
//
// BUILDING FROM SOURCE CODE
// =========================
// Use the Visual Studio 2015 "cnc_warp.sln" to build it.
//
// SOURCE CODE DEPENDENCIES
// ========================
// - CNCUSBControllerAPI.tlb: PlanetCNC's own COM interface type library for CNC USB Controller.
//
// LICENSE AND WARRANTY
// ====================
// This code is GPL v3 licensed and may be used freely under its terms. This software comes without any warranty, 
// so use it at your own risk. I am not liable for any damage caused directly or indirectly by this software.

// Include headers
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>
#include <tchar.h>
#include <time.h>
#include <cstring>
#include <fstream>
#include <sstream>
#include <iostream>

#ifdef ReportEvent
#undef ReportEvent
#endif



// Include type libraries, necessary to access CNC USB Controller via its COM interface
#import "mscorlib.tlb"
#import "CNCUSBControllerAPI.tlb"




// Typedefs and namespaces
using namespace CNCUSBControllerAPI;



// Configuration globals, fixed after initialization
static const char* s_probe_start_command = "\"C:\\Program Files (x86)\\CNC USB Controller\\CNCUSBController.exe\" -mdi /459";
static const char* s_probe_stop_command = "\"C:\\Program Files (x86)\\CNC USB Controller\\CNCUSBController.exe\" -mdi /406";
const char* output_filename = "cnc_warp_probe_output.txt";
double probe_offset_x = 0.0;
double probe_offset_y = 0.0;
double probe_area_x = 40.0;
double probe_area_y = 40.0;
double move_speed_xy = 1000.0;
int probe_repeats = 3;
int probe_steps_x = 4;
int probe_steps_y = 4;



// Global describing the current state of the process
double probe_begin_x, probe_begin_y, probe_begin_z;
double probe_max_z = -1000.0;
int probe_index_x, probe_index_y;
int remaining_probe_repeats;
DWORD move_start_time = 0;
std::ofstream file_stream;
ICNCPtr cnc_api_ptr;

enum EProbeState
{
	PROBE_STATE_INIT,
	PROBE_STATE_MOVING,
	PROBE_STATE_PROBING,
	PROBE_STATE_HOMING1,
	PROBE_STATE_HOMING2,
	PROBE_STATE_HOMING3,
	PROBE_STATE_DONE,
};

EProbeState probe_state;



// Try to access and initialize the COM interface to CNC USB Controller.
bool init_cnc_api()
{
	// Create the COM instance
	std::cout << "Instancing CNC API... ";
	CoInitialize(0);
	cnc_api_ptr.CreateInstance(__uuidof(CNC));
	if (cnc_api_ptr == 0)
	{
		std::cout << "FAILED. COM Object not registered?\n";
		return false;
	}
	std::cout << "OK\n";

	// Initialize the instance
	std::cout <<  "Initializing CNC API... ";
	long result = cnc_api_ptr->Initialize();
	if( result == 0 )
	{
		cnc_api_ptr = 0;
		std::cout << "FAILED. Attached debugger?\n";
		return false;
	}
	std::cout << "OK\n";

	// Make sure its connected to a valid machine
	if (!cnc_api_ptr->GetLicenseValid())
	{
		std::cout << "INVALID LICENSE\n";
		return false;
	}

	return true;
}



// Break down the connection to the COM interface
void uninit_cnc_api()
{
	if (cnc_api_ptr != 0)
	{
		cnc_api_ptr->Dispose();
		cnc_api_ptr->Release();
		cnc_api_ptr = 0;
	}

	CoUninitialize();
}



// Start a probing cycle at the current position using the command-line interface to CNC USB Controller
void start_probe()
{
	printf("probe %d %d: ", probe_index_x, probe_index_y);
	system(s_probe_start_command);
	move_start_time = GetTickCount();
	probe_state = PROBE_STATE_PROBING;
}



// Finish the probing cycle by cancelling it so that it doesn't use the measured Z as its new work space Z origin.
void stop_probe()
{
	system(s_probe_stop_command);
}



// The connection to CNC USB Controller is not truely in real-time, so commands and status updates can take over a second
// before being picked. This function returns true when the machine has been sent a command and is currently (still) processing it.
bool is_moving()
{
	DWORD current_time = GetTickCount();
	if (current_time - move_start_time > 2500)
	{
		double speed = cnc_api_ptr->GetSpeed();
		if (speed == 0)
			return false;
	}

	return true;
}



// Query the current work space X position via the COM interface.
double get_latest_x()
{
	ICoord *pCoord = cnc_api_ptr->GetPosition();
	if (!pCoord)
	{
		std::cout << "Failed to retrieve coordinates. Terminating...\n";
		exit(1);
	}

	double x = pCoord->Getx();
	pCoord->Release();
	return x;
}



// Query the current work space Y position via the COM interface.
double get_latest_y()
{
	ICoord *pCoord = cnc_api_ptr->GetPosition();
	if (!pCoord)
	{
		std::cout << "Failed to retrieve coordinates. Terminating...\n";
		exit(1);
	}

	double y = pCoord->Gety();
	pCoord->Release();
	return y;
}


// Query the current work space Z position via the COM interface.
double get_latest_z()
{
	ICoord *pCoord = cnc_api_ptr->GetPosition();
	if (!pCoord)
	{
		std::cout << "Failed to retrieve coordinates. Terminating...\n";
		exit(1);
	}

	double z = pCoord->Getz();
	pCoord->Release();
	return z;
}



// Send a command via the COM interface to move to the given XY work space position.
void goto_xy(double x, double y)
{
	double current_x = get_latest_x();
	double current_y = get_latest_y();
	if (x != current_x || y != current_y)
	{
		move_start_time = GetTickCount();
		cnc_api_ptr->SendMoveDelta2(x - current_x, y - current_y, 0, 0, 0, 0, 0, 0, 0, UnitsEnum_Millimeters, move_speed_xy);
	}
}



// Send a command via the COM interface to move to the Z height to the given work space coordinate.
void goto_z(double z)
{
	double current_z = get_latest_z();
	if (z != current_z)
	{
		move_start_time = GetTickCount();
		cnc_api_ptr->SendMoveDelta2(0, 0, z - current_z, 0, 0, 0, 0, 0, 0, UnitsEnum_Millimeters, move_speed_xy);
	}
}



// Write the last probe's X, Y and Z position to the output file.
void output_current_probe()
{
	double probe_z = get_latest_z();
			
	std::stringstream ss;
	double relative_x = probe_area_x * probe_index_x / max(1, probe_steps_x - 1) + probe_offset_x;
	double relative_y = probe_area_y * probe_index_y / max(1, probe_steps_y - 1) + probe_offset_y;
	ss << relative_x << " " << relative_y << " " << probe_z << std::endl;
	std::cout << ss.str();
	file_stream << ss.str();
	file_stream.flush();

	probe_max_z = max(probe_max_z, probe_z);
}



// Initialize the connection to CNC USB Controller and set the state machine to move to the first probe location.
void init()
{
	if (!init_cnc_api())
	{
		std::cout << "Fatal error\n";
		exit(-1);
	}

	file_stream.open(output_filename);

	move_start_time = GetTickCount();
	probe_begin_x = get_latest_x();
	probe_begin_y = get_latest_y();
	probe_begin_z = get_latest_z();

	probe_state = PROBE_STATE_INIT;
}



// Finish the connection to CNC USB Controller and the close the written probe file.
void uninit()
{
	uninit_cnc_api();

	file_stream.close();
}



// State machine updating locations and querying height during the whole probing sequence.
void do_work()
{
	if (!is_moving())
	{
		switch (probe_state)
		{
		case PROBE_STATE_INIT:
		{
			// Start the probing sequence: Move to first XY position.
			probe_index_x = 0;
			probe_index_y = 0;

			goto_xy(probe_begin_x + probe_offset_x, probe_begin_y + probe_offset_y);
			probe_state = PROBE_STATE_MOVING;
		}
		break;

		case PROBE_STATE_MOVING:
		{
			// Finished move. Start the next probe.
			remaining_probe_repeats = probe_repeats;
			start_probe();
		}
		break;

		case PROBE_STATE_PROBING:
		{
			// Finished a probe. Store measurement and move to the next position.
			stop_probe();
				
			output_current_probe();

			if (remaining_probe_repeats > 1)
			{
				// Do another probe at the current XY location.
				start_probe();
				--remaining_probe_repeats;
			}
			else
			{
				// Figure out the next probe_index_x (between 0 and probe_steps_x - 1), and probe_index_y (between 0 and probe_steps_y - 1).
				if ((probe_index_y & 1) == 0)
				{
					if (probe_index_x < probe_steps_x - 1)
						++probe_index_x;
					else
						++probe_index_y;
				}
				else
				{
					if (probe_index_x > 0)
						--probe_index_x;
					else
						++probe_index_y;
				}

				if (probe_index_y == probe_steps_y)
				{
					// After having finished the vey last probe, go up a bit before going to the original XY location.
					goto_z(max(probe_max_z + 2.0f, probe_begin_z));
					probe_state = PROBE_STATE_HOMING1;
				}
				else
				{
					// There are more probes to be done: Move to the next XY probe location based on probe_index_x and probe_index_y.
					double target_x = probe_begin_x + probe_area_x * probe_index_x / max(1, probe_steps_x - 1) + probe_offset_x;
					double target_y = probe_begin_y + probe_area_y * probe_index_y / max(1, probe_steps_y - 1) + probe_offset_y;
					goto_xy(target_x, target_y);
					probe_state = PROBE_STATE_MOVING;
				}
			}
		}
		break;

		case PROBE_STATE_HOMING1:
		{
			// Finished going to safe Z height. Go to the begin XY position.
			goto_xy(probe_begin_x, probe_begin_y);
			probe_state = PROBE_STATE_HOMING2;
		}
		break;

		case PROBE_STATE_HOMING2:
		{	
			// Finished going to the begin XY position. Go to the begin Z position.
			goto_z(probe_begin_z);
			probe_state = PROBE_STATE_HOMING3;
		}
		break;

		case PROBE_STATE_HOMING3:
		{	
			// Done!
			probe_state = PROBE_STATE_DONE;
		}
		break;
		}
	}
}



// Entry point to the application.
int main(int argc, char** argv)
{
	// Verify that the given command-line arguments fits the expected format.
	if (argc != 10)
	{
		std::cout << "SYNTAX ERROR: Expected cnc_warp_probe.exe <output_filename> <min_x> <min_y> <max_x> <max_y> <move_speed_xy> <probe_steps_x> <probe_steps_y> <probe_repeats>\n";
		std::cout << "Giliam de Carpentier, 2017. See http://www.decarpentier.nl or https://bitbucket.org/gdecarpentier/cnc_warp for details.\n";
		return -1;
	}

	output_filename = argv[1];

	double max_x	= atof(argv[4]);
	double max_y	= atof(argv[5]);
	probe_area_x	= max_x;
	probe_area_y	= max_y;

	move_speed_xy	= atof(argv[6]);
	probe_steps_x	= atoi(argv[7]);
	probe_steps_y	= atoi(argv[8]);
	probe_repeats	= atoi(argv[9]);

	if (probe_area_x < 0 || probe_area_y < 0)
	{
		std::cout << "SYNTAX ERROR: Invalid XY dimensions\n";
		return -1;
	}

	if (move_speed_xy < 0)
	{
		std::cout << "SYNTAX ERROR: Invalid move_speed_xy\n";
		return -1;
	}

	if (probe_steps_x <= 0)
	{
		std::cout << "SYNTAX ERROR: Invalid probe_steps_x\n";
		return -1;
	}

	if (probe_steps_y <= 0)
	{
		std::cout << "SYNTAX ERROR: Invalid probe_steps_y\n";
		return -1;
	}

	if (probe_repeats <= 0)
	{
		std::cout << "SYNTAX ERROR: Invalid probe_repeats\n";
		return -1;
	}

	// Initialize the connection to CNC USB Controller and prepare writing to a file.
	init();

	// Do the actual work.
	MSG msg;
	while (probe_state != PROBE_STATE_DONE)
	{
		do_work();

		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) > 0) 
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		Sleep(1);
	}

	// Cleaup up the connection to CNC USB Controller and close the output file.
	uninit();

	std::cout << "Done\n";
    return 0;
}
