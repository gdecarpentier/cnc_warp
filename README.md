# Auto leveling toolset for CNC USB Controller
Giliam de Carpentier, 2017. 
http://www.decarpentier.nl

---

This toolset allows me to do auto-leveling on a PlanetCNC's "CNC USB Controller"-based CNC machine. For more details, see http://www.decarpentier.nl/adding-auto-leveling. 

To use it, first call the cnc_warp_probe tool to control CNC USB Controller such that it probes the height inside a given area. See /cnc_warp_probe/INSTRUCTIONS.txt for details.

Then use the cnc_warp_gcode tool (via gcode_warp_gcode_clipboard.bat) to use the collected probe data and adjust any GCode loaded in CNC USB Controller. See /cnc_warp_gcode/INSTRUCTIONS.txt for details.

A demonstration video is available here:


[![IMAGE ALT TEXT HERE](http://img.youtube.com/vi/RYA5ivhm8xk/0.jpg)](http://www.youtube.com/watch?v=RYA5ivhm8xk)


The source code for cnc_warp_probe and cnc_warp_gcode is in this repository, and is provided under the GPL v3 license. See LICENSE.txt for details. 

This software comes without any warranty, so use it at your own risk. I am not liable for any damage caused directly or indirectly by this software.

Executables built from the latest source code may be downloaded from http://www.decarpentier.nl/adding-auto-leveling#downloads.