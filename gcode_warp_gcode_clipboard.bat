REM Giliam de Carpentier, 2017. http://www.decarpentier.nl
@cd /d %~dp0
clip /r gcode_input.txt
cnc_warp_gcode\cnc_warp_gcode.exe probe_measurements.txt gcode_input.txt gcode_output.txt
clip gcode_output.txt

pause